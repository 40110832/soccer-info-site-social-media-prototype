from flask import Flask
app = Flask(__name__)

import myapplication.views
import myapplication.premierleague
import myapplication.bundesliga
import myapplication.laliga
import myapplication.error