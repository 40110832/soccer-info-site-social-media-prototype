from myapplication import app
from flask import render_template, url_for, redirect

bm = {'Info': 'Short Info: FC Bayern, is a German sports club based in Munich, Bavaria. It is best known for its professional football team, which plays in the Bundesliga, the top tier of the German football league system, and is the most successful club in German football history, having won a record 25 national titles and 17 national cups.', 'Manager': 'Manager: Pep Guardiola', 'Stadium': 'Stadium: Allianz Arena', 'Location': 'Location: Munich', 'Founded': 'Founded 1900', 'Leagues': 'Leagues: DFB-Pokal, UEFA Champions League, Bundesliga', 'Name': 'Bayern Munchen'};
bd = {'Info': 'Short Info: Borussia Dortmund, or BVB, is a German sports club based in Dortmund, North Rhine-Westphalia. The football team is part of a large membership-based sports club with more than 115,000 members, making BVB the third largest sports club by membership in Germany. Dortmund plays in the Bundesliga, the top tier of the German football league system. Dortmund is one of the most successful clubs in German football history.', 'Manager': 'Manager: Thomas Tuchel', 'Stadium': 'Stadium: Westfalenstadion', 'Location': 'Location: Dortmund', 'Founded': 'Founded 1909', 'Leagues': 'Leagues: DFB-Pokal, UEFA Champions League, Bundesliga', 'Name': 'Borussia Dortmund'};
fs = {'Info': 'Short Info: FC Shalke 04 is a professional German association-football club and multi-sports club originally from the Schalke district of Gelsenkirchen, North Rhine-Westphalia. The "04" in the club\'s name derives from its formation in 1904. Schalke has long been one of the most popular professional football teams and multi-sports club in Germany, even though major successes have been rare since the club\'s heyday in the 1930s and early 1940s. Schalke play in the Bundesliga, the top tier of the German football league system.', 'Manager': 'Manager: Andre Breitenreiter', 'Stadium': 'Stadium: Veltins-Arena', 'Location': 'Location: Gelsenkirchen', 'Founded': 'Founded 1904', 'Leagues': 'Leagues: DFB-Pokal, UEFA Champions League, Bundesliga', 'Name': 'FC Shalke 04'};

@app.route("/bundesliga/bayernmunich")
def bayernmunich():
  return render_template('teams.html', teams=bm)

@app.route("/bundesliga/borussiadortmund")
def borussiadortmund():
  return render_template('teams.html', teams=bd)

@app.route("/bundesliga/fcshalke04")
def fcshalke04():
  return render_template('teams.html', teams=fs)
