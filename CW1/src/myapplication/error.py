from myapplication import app
from flask import render_template, url_for, redirect

err = {'Info': 'The requested URL was not found on the server.'};

@app.errorhandler(404)
def page_not_found(error):
  return render_template('error.html', err=err)
