from myapplication import app
from flask import render_template, url_for, redirect

bar = {'Info': 'Short Info: Futbol Club Barcelona, also known as Barcelona and familiarly as Barca, is a professional football club, based in Barcelona, Catalonia, Spain.', 'Manager': 'Manager: Luis Enrique', 'Stadium': 'Stadium: Camp Nou', 'Location': 'Location: Barcelona', 'Founded': 'Founded: 1899', 'Leagues': 'Leagues: Liga BBVA', 'Name': 'Barcelona'};
rm = {'Info': 'Short Info: Real Madrid Club de Futbol, commonly known as Real Madrid, or simply as Real, is a professional football club based in Madrid, Spain. Founded in 1902 as Madrid Football Club, the team has traditionally worn a white home kit since inception.', 'Manager': 'Manager: Rafael Benitez', 'Stadium': 'Stadium: Santiago Bernabeu Stadium', 'Location': 'Location: Madrid', 'Founded': 'Founded: 1902', 'Leagues': 'Leagues: Liga BBVA', 'Name': 'Real Madrid'};
am = {'Info': 'Short Info: Club Atletico de Madrid, SAD commonly known as Atletico de Madrid or Atletico is a Spanish professional football club based in Madrid and is currently playing in La Liga.', 'Manager': 'Manager: Diego Simeone', 'Stadium': 'Stadium: Vicente Calderon Stadium', 'Location': 'Location: Madrid', 'Founded': 'Founded: 1903', 'Leagues': 'Leagues: Liga BBVA', 'Name': 'Atletico Madrid'};

@app.route("/laliga/barcelona")
def barcelona():
  return render_template('teams.html', teams=bar)

@app.route("/laliga/realmadrid")
def realmadrid():
  return render_template('teams.html', teams=rm)

@app.route("/laliga/atleticomadrid")
def atleticomadrid():
  return render_template('teams.html', teams=am)
