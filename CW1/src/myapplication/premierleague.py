from myapplication import app
from flask import render_template, url_for, redirect

ars = {'Info': 'Short Info: Arsenal Football Club is a professional football club based in Holloway, London. The club play in the Premier League, the top flight of English football.', 'Manager': 'Manager: Arsene Wenger', 'Stadium': 'Stadium: Emirates Stadium', 'Location': 'Location: London', 'Founded': 'Founded 1886, Woolwich, London', 'Leagues': 'Leagues: Premier League, FA Cup, Football League Cup', 'Name': 'Arsenal'};
mu = {'Info': 'Short Info: Manchester United Football Club is a professional football club based in Old Trafford, Greater Manchester, England, that currently competes in the Premier League, the top flight of English football.', 'Manager': 'Manager: Louis van Gaal', 'Stadium': 'Stadium: Old Trafford', 'Location': 'Location: Manchester', 'Founded': 'Founded 1878', 'Leagues': 'Leagues: Premier League, FA Cup, Football League Cup', 'Name': 'Manchester United'};
lpool = {'Info': 'Short Info: Liverpool Football Club are a Premier League football club based in Liverpool, England. The club have won more European trophies than any other English team with five European Cups, three UEFA Cups and three UEFA Super Cups.', 'Manager': 'Manager: Jurgen Klopp', 'Stadium': 'Stadium: Anfield', 'Location': 'Location: Liverpool', 'Founded': 'Founded 1892', 'Leagues': 'Leagues: Premier League, FA Cup, Football League Cup', 'Name': 'Liverpoool'};

@app.route("/premierleague/arsenal")
def arsenal():
  return render_template('teams.html', teams=ars)

@app.route("/premierleague/liverpool")
def liverpool():
  return render_template('teams.html', teams=lpool)

@app.route("/premierleague/manutd")
def manutd():
  return render_template('teams.html', teams=mu)


