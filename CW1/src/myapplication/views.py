from myapplication import app
from flask import render_template, url_for, redirect

h = {'Info': 'Welcome to FLC(Football Leagues Catalogue)'}
pl = {'Info': 'The Premier League is an English professional league for men\'s association football clubs. At the top of the English football league system, it is the country\'s primary football competition. Contested by 20 clubs, it operates on a system of promotion and relegation with the Football League. Besides English clubs, the Welsh clubs that compete in the English football league system can also qualify to play.'};
bl = {'Info': 'The Bundesliga is a professional association football league in Germany and the football league with the highest average stadium attendance worldwide. At the top of the German football league system, the Bundesliga is Germany\'s primary football competition. The Bundesliga is contested by 18 teams and operates on a system of promotion and relegation with the 2. Bundesliga. Seasons run from August to May. Most games are played on Saturdays and Sundays, with a few games played during weekdays. All of the Bundesliga clubs qualify for the DFB-Pokal. The winner of the Bundesliga qualifies for the DFL-Supercup.'};
ll = {'Info': 'Laliga is the top professional association football division of the Spanish football league system. It is officially named Liga BBVA (BBVA League) for sponsorship reasons. It is contested by 20 teams, with the three lowest placed teams relegated to the Segunda Division and replaced by the top two teams in that division plus the winner of a play-off.'};
@app.route("/")
def home():
   return render_template('home.html', h=h) 

@app.route("/premierleague/")
def premierleague():
 return render_template('premierleague.html', pl=pl)

@app.route("/bundesliga/")
def bundesliga():
  return render_template('bundesliga.html', bl=bl)

@app.route("/laliga/")
def primeradivision():
  return render_template('laliga.html', ll=ll)


