import ConfigParser
import sqlite3
import time
import os
from flask import Flask, request, session, url_for, redirect, render_template, abort, g, flash
from werkzeug import check_password_hash, generate_password_hash
from hashlib import md5
from datetime import datetime, timedelta

app = Flask(__name__)
dblocation = 'var/sqlite3.db'

#Configuration

def init(app):
    config = ConfigParser.ConfigParser()
    config_location = "etc/config.cfg"
    try:
        config.read(config_location)

        app.config['DEBUG'] = config.get("config", "debug")
        app.config['ip_address'] = config.get("config", "ip_address")
        app.config['port'] = config.get("config", "port")
        app.config['url'] = config.get("config", "url")
        app.secret_key = os.urandom(24)
        app.permanent_session_lifetime = timedelta(seconds=60)

    except:
        print ('Could not read config: '), config_location


# Database functions

#get access to database connection
def get_db():
    db = getattr(g, 'db', None)
    if db is None:
        db = sqlite3.connect(dblocation)
        g.db = db
        db.row_factory = sqlite3.Row
    return db

#initialize schema
def init_db():
    with app.app_context():
        db = get_db() 
        with app.open_resource('schema.sql', mode='r') as f:
            db.cursor().executescript(f.read())
        db.commit()

#combines getting the cursor,executing and fetching tha results
def query_db(query, args=(), one=False):  
    cur = get_db().execute(query, args)
    rv = cur.fetchall()
    return (rv[0] if rv else None) if one else rv

#check before each request that user is logged in
#set up g.user global
@app.before_request
def before_request():
    g.user = None
    if 'user_id' in session:
        g.user = query_db('select * from user where user_id = ?',
                          [session['user_id']], one=True)


#after each request
@app.teardown_appcontext
def close_db_connection(exception):
    db = getattr(g, 'db', None)
    if db is None:
        db.close()

#query for username
def get_userid(username):
    rv = query_db('select user_id from user where username = ?',
                  [username], one=True)
    return rv[0] if rv else None

#query for email
def get_email(email):
    rv = query_db('select email from user where email = ?',
                  [email], one=True)
    return rv[0] if rv else None

#format date and own jinja filter
@app.template_filter('format')
def date(current_timestamp):
    return datetime.fromtimestamp(current_timestamp).strftime('%Y-%m-%d %H:%M:%S')

#own jinja filter for avatar
@app.template_filter('avatar')
def avatar(self, size):
    return 'http://www.gravatar.com/avatar/%s?d=identicon&s=%d' % (md5(self.strip().lower().encode('utf-8')).hexdigest(), size)

#Routes

@app.route('/')
def home():
            return redirect(url_for('login'))  


@app.route('/register', methods=['GET', 'POST'])
def register():
    err = None
    if request.method == 'POST':
        if not request.form['username']:
            err = 'Please write your name'
        elif not request.form['email']:
            err = 'Please write your E-mail address'
        elif '@' not in request.form['email']:
            err = 'Your E-mail is not valid'
        elif not request.form['password']:
            err = 'You have to enter a password'
        elif request.form['password'] != request.form['password2']:
            err = 'Password1 and Password2 do not match'
        elif get_userid(request.form['username']) is not None:
            err = 'The username is already taken'
        elif get_email(request.form['email']) is not None:
            err = 'The Email is already taken'
        else:
            db = get_db()
            db.execute('''insert into user(
              username, email, password) values (?, ?, ?)''',
              [request.form['username'], request.form['email'],
               generate_password_hash(request.form['password'])])
            db.commit()
            flash('You were successfuly registered, try to login!')
            return redirect(url_for('login'))              
    return render_template('signup.html', err=err)


@app.route('/login', methods=['GET', 'POST'])
def login():
    err = None
    if request.method == 'POST':
        user = query_db('''select * from user where
            username = ?''', [request.form['username']], one=True)
        if user is None:
            err = 'Your username is wrong'
        elif not check_password_hash(user['password'],
                                     request.form['password']):
            err = 'Your password is wrong'
        else:
            flash('You just logged in')
            session['user_id'] = user['user_id']
            return redirect(url_for('addmessage'))
    return render_template('login.html', err=err)

@app.route('/addmessage', methods=['GET', 'POST'])
def addmessage():
    if not g.user:
        abort(401)
    if request.method == 'POST':
        db = get_db()
        db.execute('''insert into message (publisher, message, date)
          values (?, ?, ?)''', (session['user_id'], request.form['message'],
                                int(time.time())))
        db.commit()
        flash('Message saved')
    return render_template('addmessage.html')

@app.route('/dashboard')
def dashboard():
    if not g.user:
        return redirect(url_for('register'))    
    return render_template('dashboard.html', messages=query_db('''
        select * from message, user
        where message.publisher = user.user_id
        order by message.date'''))


@app.route('/mypage')
def mypage():
    if not g.user:
        return redirect(url_for('register'))
    return render_template('dashboard.html', messages=query_db('''
        select * from message, user
        where message.publisher = user.user_id and user.user_id = ?
        order by message.date''',[session['user_id']]))  

@app.route('/users')
def users():
    if not g.user:
        return redirect(url_for('register'))
    return render_template('users.html', users=query_db('''
        select * from user'''))     

@app.route('/<username>')
def userpersonalpage(username):
    if not g.user:
        return redirect(url_for('register'))
    selectedusername = query_db('select * from user where username = ?',
                            [username], one=True)
    if selectedusername is None:
        abort(404)
    return render_template('dashboard.html', messages=query_db('''
            select * from message, user where
            user.user_id = message.publisher and user.user_id = ?
            order by message.date''',[selectedusername['user_id']]))
    

@app.route('/logout')
def logout():
    flash('You were logged out')
    session.pop('user_id', None)
    return redirect(url_for('login'))

@app.errorhandler(404)
def page_not_found(error):
  err = 'The requested URL was not found on the server.'
  return render_template('error.html', err=err)


if __name__ == '__main__':
    init(app)
    app.run(
        host = app.config['ip_address'],
        port = int(app.config['port'])
    )
