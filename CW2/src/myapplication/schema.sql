DROP TABLE if EXISTS user;
CREATE TABLE user (
  user_id integer PRIMARY KEY autoincrement,
  username text NOT NULL,
  email text NOT NULL,
  password text NOT NULL
);

DROP TABLE if EXISTS message;
CREATE TABLE message (
  message_id integer PRIMARY KEY autoincrement,
  publisher integer NOT NULL,
  message text NOT NULL,
  date integer
);