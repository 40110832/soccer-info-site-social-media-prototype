# README #

**CW1**

To execute the application, cd CW1/src and execute the command python run.py

**CW2**

First redirect to CW2/src and check that you have all libraries installed, if not proceed to Step1, otherwise skip to Step2.

**Step1**

Assuming that you have Pip and VirtualEnv installed and your current folder is CW2/src, create a new virtualenv: $ virtualenv env

Start your virtualenv and install the external libraries using requirements.txt, $ pip install -r requirements.txt

**Step2**

cd CW2/src/myapplication, first execute the command: python init_db.py and then execute the command python run.py

Open the browser and navigate to http://localhost:5000/