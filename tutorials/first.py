#print "Hello Napier"

from flask import Flask
import tvdb_api
app= Flask(__name__)

t = tvdb_api.Tvdb()
episode = t['Breaking Bad'][1][3]
#print episode['episodename']

@app.route("/")
def root():
    return episode['episodename']

if __name__ == "__main__":
  app.run(host='0.0.0.0', debug=True)

